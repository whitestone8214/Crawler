/*
	Copyright (C) 2018 Minho Jo <whitestone8214@gmail.com>
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


exports.version = ['0', '2', '1'] // 0.2.1


var _filesystem = require('fs')
var _path = require('path')
var _process = require('process')
var _processChild = require('child_process')


class whiteline_file {
	constructor() {}
	
	here() {return _process.cwd()}
	clean_path(path) {return _path.normalize(_path.resolve(path))}
	place(path) {return _path.dirname(this.clean_path(path))}
	name(path) {return _path.basename(this.clean_path(path))}
	status(path, question) {
		var _question
		if (question === 'exists') _question = _filesystem.constants.F_OK
		else if (question === 'readable') _question = _filesystem.constants.R_OK
		else if (question === 'writable') _question = _filesystem.constants.W_OK
		else if (question === 'executable') _question = _filesystem.constants.X_OK
		else return false
		
		try {_filesystem.accessSync(path, _question); return true}
		catch(x) {return false}
	}
	type(path, respect_symlink) {
		if (this.status(path, 'exists') != true) return 'none'
	
		var _1 = (respect_symlink == true) ? _filesystem.lstatSync(path) : _filesystem.statSync(path)
		if (_1.isSymbolicLink() == true) return 'symlink'
		else if (_1.isFile() == true) return 'file'
		else if (_1.isDirectory() == true) return 'directory'
		else if (_1.isBlockDevice() == true) return 'block'
		else if (_1.isCharacterDevice() == true) return 'character'
		else if (_1.isFIFO() == true) return 'fifo'
		else if (_1.isSocket() == true) return 'socket'
		else return 'unknown'
	}
	children(path) {
		if (this.type(path, false) != 'directory') return undefined
		if (this.status(path, 'readable') != true) return undefined
		
		var _children = _filesystem.readdirSync(path)
		return _children
	}
	load(path) {
		var _data
		try {_data = _filesystem.readFileSync(path); return _data}
		catch(x) {return undefined}
	}
	save(path, data) {
		try {
			_filesystem.writeFileSync(path, data)
			
			return true
		}
		catch(x) {return false}
	}
	delete(path) {
		if (this.status(path, 'exists') != true) return true
		
		if (this.type(path) == 'directory') {
			var _children = this.children(path); if (_children == undefined) return false
			for (var _child in _children) {if (this.delete(_path.join(path, _children[_child])) != true) return false}
			
			try {_filesystem.rmdirSync(path); return true}
			catch(x) {return false}
		}
		else {
			try {_filesystem.unlinkSync(path); return true}
			catch(x) {return false}
		}
	}
	create(path, type, overwrite) {
		console.log('[whiteline] file create ' + path + ' (' + type + ')')
		
		if (overwrite == true) {if (this.delete(path) != true) return false}
		
		try {
			if (type == 'directory') _filesystem.mkdirSync(path)
			else {
				var _file = _filesystem.openSync(path, 'a+')
				_filesystem.closeSync(_file)
			}
			
			return true
		}
		catch(x) {return false}
	}
	copy(from, to, overwrite) {
		console.log('[whiteline] copy ' + from + ' -> ' + to)
		if (this.status(from, 'exists') != true) return false
		if (this.overwrite == true) {if (this.delete(path) != true) return false}
		
		if (this.type(from) == 'directory') {
			if (this.create(to, 'directory') != true) return false
			
			var _children = this.children(from); if (_children == undefined) return false
			for (var _child in _children) {
				console.log('[whiteline] copy child ' + _path.join(from, _children[_child]) + ' -> ' + _path.join(to, _children[_child]))
				if (this.copy(_path.join(from, _children[_child]), _path.join(to, _children[_child]), false) != true) return false
			}
			
			return true
		}
		else {
			var _data = this.load(from); if (_data == undefined) return false
			if (this.create(to, 'file') != true) return false
			
			return this.save(to, _data)
		}
	}
	move(from, to, overwrite) {
		if (this.status(from, 'exists') != true) return false
		if (this.overwrite == true) {if (this.delete(path) != true) return false}
		
		try {_filesystem.renameSync(from, to); return true}
		catch(x) {if (x.code != 'EXDEV') return false}
		
		if (this.copy(from, to, false) != true) return false
		return this.delete(from)
	}
	execute(path) {_processChild.execSync(path)}
}
exports.file = new whiteline_file()

class whiteline_html {
	constructor() {
		this._target = null
	}
	
	create(type, id, data, width, height, x, y, whenClicked) {
		console.log('[whiteline] create() is deprecated and will be removed in 0.3.0; Use create_v2() instead')
		
		var _element
		if (type == 'entry') {
			_element = document.createElement('input')
				_element.setAttribute('type', 'text')
		}
		else if (type == 'checkbox') {
			_element = document.createElement('input')
				_element.setAttribute('type', 'checkbox')
		}
		else _element = document.createElement(type)
			if ((id != undefined) && (id != null)) _element.id = id
			if ((data != undefined) && (data != null)) _element.innerHTML = data
			if ((width != undefined) && (width != null)) _element.style.width = width + 'px'
			if ((height != undefined) && (height != null)) _element.style.height = height + 'px'
			if ((x != undefined) && (x != null)) _element.style.left = x + 'px'
			if ((y != undefined) && (y != null)) _element.style.top = y + 'px'
			if ((whenClicked != undefined) && (whenClicked != null)) _element.onclick = whenClicked
			_element.style.position = 'absolute'
			_element.style.zIndex = '1'
			
		return _element
	}
	create_v2(type, id) {
		var _element
		if (type == 'entry') {
			_element = document.createElement('input')
				_element.setAttribute('type', 'text')
		}
		else if (type == 'checkbox') {
			_element = document.createElement('input')
				_element.setAttribute('type', 'checkbox')
		}
		else _element = document.createElement(type)
			if ((id != undefined) && (id != null)) _element.id = id
			_element.style.position = 'absolute'
			_element.style.zIndex = '1'
		
		return _element
	}
	pick(id) {return document.getElementById(id)}
	pack(id, child) {
		if (this.pick(id) == null) console.log('[whiteline] WTF...')
		this.pick(id).appendChild(child)
		return this
	}
	clear(id) {
		var _element = this.pick(id)
		for (var _nth = 0; _nth < _element.childNodes.length; _nth++) {
			_element.removeChild(_element.childNodes[_nth])
		}
		_element.innerHTML = ''
		
		return this
	}
	
	target(element) {console.log('[whiteline] target() is deprecated and will be removed in 0.3.0'); this._target = element; return this}
	width(value) {console.log('[whiteline] width() is deprecated and will be removed in 0.3.0'); this._target.style.width = value + 'px'; return this}
	height(value) {console.log('[whiteline] height() is deprecated and will be removed in 0.3.0'); this._target.style.height = value + 'px'; return this}
	x(value) {console.log('[whiteline] x() is deprecated and will be removed in 0.3.0'); this._target.style.left = value + 'px'; return this}
	y(value) {console.log('[whiteline] y() is deprecated and will be removed in 0.3.0'); this._target.style.top = value + 'px'; return this}
	contents(value) {console.log('[whiteline] contents() is deprecated and will be removed in 0.3.0'); this._target.innerHTML = value; return this}
	whenTapped(value) {console.log('[whiteline] whenTapped() is deprecated and will be removed in 0.3.0'); this._target.onclick = value; return this}
	
	box(id, width, height, x, y) {
		var _box = H.create_v2('div', id)
			_box.style.width = width + 'px'; _box.style.height = height + 'px'
			_box.style.left = x + 'px'; _box.style.top = y + 'px'
			_box.style.padding = '0px'; _box.style.border = '0px'; _box.style.margin = '0px'
			
		return _box
	}
}
exports.html = new whiteline_html()
    
